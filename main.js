let offset = 0;
const slides = document.querySelector('#slides'),
    next = document.querySelector('#next'),
    prev = document.querySelector('#prev');

next.addEventListener('click', () => {
    offset += 1000;
    if (offset > 3000) {
        offset = 3000;
    }
    slides.style.left = -offset + 'px';
});

prev.addEventListener('click', () => {
    offset -= 1000;
    if (offset <= 0) {
        offset = 0;
    }
    slides.style.left = -offset + 'px';
});